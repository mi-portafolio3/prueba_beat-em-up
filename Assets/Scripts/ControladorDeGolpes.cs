using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorDeGolpes : MonoBehaviour
{
    int contadorDeGolpes;
    bool puedesGolpear;
    private float ultimoGolpe;
    private float golpesMaximos = 0.6f;

    public int danio;

    PersonajeControl personaje;

    private void Start()
    {
        personaje = GetComponentInParent<PersonajeControl>();
    }

    private void Update()
    {
        personaje.PuedesGolpear(puedesGolpear);
        if (puedesGolpear)
        {
            if (Time.time - ultimoGolpe > golpesMaximos)
            {
                contadorDeGolpes = 0;
                puedesGolpear = false;
            }
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        EnemigoControl enemigo = other.GetComponent<EnemigoControl>();

        if (enemigo != null)
        {
            puedesGolpear = true;
            ultimoGolpe = Time.time;
            contadorDeGolpes++;
            contadorDeGolpes = Mathf.Clamp(contadorDeGolpes, 0, 5);
            enemigo.RecibirDanio(danio);

            if (contadorDeGolpes == 5)
            {
                enemigo.Lanzamiento(true);
            }
        }
    }
}
