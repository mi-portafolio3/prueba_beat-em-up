using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChecadorDeSuelo : MonoBehaviour
{
    PersonajeControl jugador;

    private bool verificador;

    // Start is called before the first frame update
    void Start()
    {
        jugador = GetComponentInParent<PersonajeControl>();
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Piso"))
        {
            verificador = true;
            jugador.ChecarSuelo(verificador);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Piso"))
        {
            verificador = false;
            jugador.ChecarSuelo(verificador);
        }
    }
}
