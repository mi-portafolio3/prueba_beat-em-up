using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonajeControl : MonoBehaviour
{
    Rigidbody rbd;
    Animator anim;

    private bool muerto = false;
    private bool estasEnElSuelo;
    private bool voltearALosLados;
    private bool salto;
    private bool GolpeandoEnemigo;
    private bool recibioDanio;
    private bool lazamiento;
    public bool tomarEnemigo;

    public float velocidad = 4f;
    public float fuerzaDeSalto = 400f;
    public float altoMinimo;
    public float altoMaximo;
    public float tiempoParaRecuperacion = 0.5f;

    private float velocidadActual;
    private float fuerzaDeSaltoActual;
    private float ultimoClick;
    private float golpesMaximos = 0.6f;
    private float tiempoDeRecuperacion;

    public int contadorDeClicks;
    public int vida;
    private int VidaActual;

    // Start is called before the first frame update
    void Start()
    {
        rbd = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();

        velocidadActual = velocidad;
        fuerzaDeSaltoActual = fuerzaDeSalto;
        VidaActual = vida;
    }

    // Update is called once per frame
    void Update()
    {
        Controles();
        Animaciones();
        Recuperacion();
    }

    private void FixedUpdate()
    {
        if (!muerto)
        {
            Movimiento();
        }
    }

    private void Movimiento()
    {
        float fuerzaHorizontal = Input.GetAxisRaw("Horizontal");
        float fuerzaVertical = Input.GetAxisRaw("Vertical");

        if (!estasEnElSuelo)
        {
            fuerzaVertical = 0;
        }

        if (!recibioDanio)
        {
            rbd.velocity = new Vector3(fuerzaHorizontal * velocidadActual, rbd.velocity.y, fuerzaVertical * velocidadActual);

            if (fuerzaHorizontal < 0 && !voltearALosLados)
            {
                RotarImagen();

            }
            else if (fuerzaHorizontal > 0 && voltearALosLados)
            {
                RotarImagen();
            }

            if (salto)
            {
                salto = false;
                rbd.AddForce(Vector3.up * fuerzaDeSaltoActual);
            }
        }
        LimitarMovimiento();
    }

    private void LimitarMovimiento()
    {
        float anchoMinimo = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 10)).x;
        float anchoMaximo = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0, 10)).x;

        rbd.position = new Vector3(Mathf.Clamp(rbd.position.x, anchoMinimo + 1, anchoMaximo - 1),
            rbd.position.y,
            Mathf.Clamp(rbd.position.z, altoMinimo + 1, altoMaximo - 1));
    }

    private void RotarImagen()
    {
        voltearALosLados = !voltearALosLados;
        Vector3 escala = transform.localScale;
        escala.x *= -1;
        transform.localScale = escala;
    }

    //nuevo codigo
    public void RecibirDanio(int valorVida)
    {
        if (!muerto)
        {
            recibioDanio = true;
            VidaActual -= valorVida;

            if (VidaActual <= 0)
            {
                velocidadActual = 0;
                rbd.AddRelativeForce(new Vector3(5, 3, 0), ForceMode.Impulse);
                lazamiento = true;
                muerto = true;
            }
        }
    }

    public void Lanzamiento(bool miLazamiento)
    {
        if (VidaActual > 0 && !lazamiento)
        {
            lazamiento = miLazamiento;
            velocidadActual = 0;
            fuerzaDeSaltoActual = 0;
            rbd.AddRelativeForce(new Vector3(5, 3, 0), ForceMode.Impulse);
        }
    }

    private void Recuperacion()
    {
        if (recibioDanio && !muerto && !lazamiento)
        {
            tiempoDeRecuperacion += Time.deltaTime;
            velocidadActual = 0;

            if (tiempoDeRecuperacion >= tiempoParaRecuperacion)
            {
                velocidadActual = velocidad;
                recibioDanio = false;
                tiempoDeRecuperacion = 0;
            }
        }
    }

    private void Levantar()
    {
        lazamiento = false;
        transform.position = new Vector3(transform.position.x, 8.9f, transform.position.z);
    }

    private void ReiniciarVelocidad()
    {
        velocidadActual = velocidad;
        fuerzaDeSaltoActual = fuerzaDeSalto;
    }

    //fin del codigo nuevo

    public void ChecarSuelo(bool verificador)
    {
        estasEnElSuelo = verificador;
    }

    public void PuedesGolpear(bool puedesGolpear)
    {
        GolpeandoEnemigo = puedesGolpear;
    }

    void Controles()
    {
        if (Time.time - ultimoClick > golpesMaximos && !recibioDanio && !tomarEnemigo)
        {
            contadorDeClicks = 0;
            velocidadActual = velocidad;
            fuerzaDeSaltoActual = fuerzaDeSalto;
        }

        if (Input.GetButtonDown("Jump") && estasEnElSuelo && !recibioDanio && !tomarEnemigo)
        {
            salto = true;
        }

        if (Input.GetMouseButtonDown(0) && estasEnElSuelo && !recibioDanio && !tomarEnemigo)
        {
            velocidadActual = 0;
            fuerzaDeSaltoActual = 0;
            ultimoClick = Time.time;
            contadorDeClicks++;

            if (contadorDeClicks == 1)
            {
                anim.SetBool("A1", true);
            }
            contadorDeClicks = Mathf.Clamp(contadorDeClicks, 0, 5);
        }

        if (Input.GetMouseButtonDown(1) && estasEnElSuelo && !recibioDanio)
        {
            velocidadActual = 0;
            fuerzaDeSaltoActual = 0;
            tomarEnemigo = true;
        }
        else if (Input.GetMouseButtonUp(1) && estasEnElSuelo || recibioDanio)
        {
            velocidadActual = velocidad;
            fuerzaDeSaltoActual = fuerzaDeSalto;
            tomarEnemigo = false;
        }
    }

    private void Retorno1()
    {
        if (contadorDeClicks >= 2 && GolpeandoEnemigo && !recibioDanio)
        {
            anim.SetBool("A2", true);
        }
        else
        {
            anim.SetBool("A1", false);
            contadorDeClicks = 0;
        }
    }

    private void Retorno2()
    {
        if (contadorDeClicks >= 3 && GolpeandoEnemigo && !recibioDanio)
        {
            anim.SetBool("A3", true);
        }
        else
        {
            anim.SetBool("A2", false);
            anim.SetBool("A1", false);
            contadorDeClicks = 0;
        }
    }

    private void Retorno3()
    {
        if (contadorDeClicks >= 4 && GolpeandoEnemigo && !recibioDanio)
        {
            anim.SetBool("A4", true);
        }
        else
        {
            anim.SetBool("A3", false);
            anim.SetBool("A2", false);
            anim.SetBool("A1", false);
            contadorDeClicks = 0;
        }
    }

    private void Retorno4()
    {
        if (contadorDeClicks >= 5 && GolpeandoEnemigo && !recibioDanio)
        {
            anim.SetBool("A5", true);
        }
        else
        {
            anim.SetBool("A4", false);
            anim.SetBool("A3", false);
            anim.SetBool("A2", false);
            anim.SetBool("A1", false);
            contadorDeClicks = 0;
        }
    }

    private void Retorno5()
    {
        anim.SetBool("A5", false);
        anim.SetBool("A4", false);
        anim.SetBool("A3", false);
        anim.SetBool("A2", false);
        anim.SetBool("A1", false);
        contadorDeClicks = 0;
    }



    void Animaciones()
    {
        anim.SetBool("Salto", estasEnElSuelo);
        anim.SetFloat("Camina", Mathf.Abs(rbd.velocity.magnitude));

        //nuevo Codigo
        anim.SetBool("Lanzamiento", lazamiento);
        anim.SetBool("Muerto", muerto);
        anim.SetBool("Hit", recibioDanio);
        anim.SetBool("TomarEnemigo", tomarEnemigo);
    }

}
