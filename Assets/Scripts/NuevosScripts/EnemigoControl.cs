using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoControl : MonoBehaviour
{
    Animator anim;
    Rigidbody rbd;

    Transform objetivo;

    private bool estasEnElSUelo;
    private bool mirarAlJugador;
    private bool muerto;
    private bool recibioDanio;
    private bool lazamiento;
    private bool LanzaEntidad;

    public bool noMover;

    private float velocidadActal;
    private float fuerzaHorizontal;
    private float fuerzaVertical;
    private float tiempoDeRecuperacion;
    private float siguienteAtaque;

    public float velocidad = 4f;
    public float altoMinimo;
    public float altoMaximo;
    public float tiempoParaRecuperacion = 0.5f;
    public float tasaDeAtaque;

    public int vida;
    private int VidaActual;
    private int numeroDeGolpe = 0;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        rbd = GetComponent<Rigidbody>();

        objetivo = FindObjectOfType<PersonajeControl>().transform;

        velocidadActal = velocidad;
        VidaActual = vida;
    }

    // Update is called once per frame
    void Update()
    {
        MirarAlJugador();
        Recuperacion();
        Animaciones();
    }

    private void FixedUpdate()
    {
        if (!muerto && !noMover)
        {
            Movimientos();
        }
    }

    private void MirarAlJugador()
    {
        mirarAlJugador = (objetivo.position.x > transform.position.x) ? false : true;


        if (mirarAlJugador)
        {
            transform.eulerAngles = new Vector3(0, 180, 0);
        }
        else
        {
            transform.eulerAngles = new Vector3(0, 0, 0);
        }
    }

    private void Movimientos()
    {
        Vector3 distanciaAlObjetivo = objetivo.position - transform.position;

        fuerzaHorizontal = distanciaAlObjetivo.x / Mathf.Abs(distanciaAlObjetivo.x);
        fuerzaVertical = distanciaAlObjetivo.z / Mathf.Abs(distanciaAlObjetivo.z);

        if (Mathf.Abs(distanciaAlObjetivo.x) < 2.9f)
        {
            fuerzaHorizontal = 0;
        }

        if (Mathf.Abs(distanciaAlObjetivo.z) < 0.1f)
        {
            fuerzaVertical = 0;
        }

        if (!recibioDanio)
        {
            rbd.velocity = new Vector3(fuerzaHorizontal * velocidadActal, 0, fuerzaVertical * velocidadActal);

            if (Mathf.Abs(distanciaAlObjetivo.x) < 2.9f && Mathf.Abs(distanciaAlObjetivo.z) < 0.1f)
            {
                Golpear();
            }
        }

        LimitarMovimiento();
    }

    private void Golpear()
    {
        numeroDeGolpe = Random.Range(1, 5);

        if (numeroDeGolpe <= 4 && Time.time > siguienteAtaque)
        {
            anim.Play("Golpe " + numeroDeGolpe);
            velocidadActal = 0;
            siguienteAtaque = Time.time + tasaDeAtaque;

            if ((numeroDeGolpe == 3 && !LanzaEntidad) || (numeroDeGolpe == 4 && !LanzaEntidad))
            {
                LanzaEntidad = true;
            }
            else {

                LanzaEntidad = false;

            }
        }
    }

    public bool lanzarEntidad()
    {
        return LanzaEntidad;
    }

    private void LimitarMovimiento()
    {
        rbd.position = new Vector3(
            rbd.position.x,
            rbd.position.y,
            Mathf.Clamp(rbd.position.z, altoMinimo + 1, altoMaximo - 1));
    }

    public void RecibirDanio(int valorVida)
    {
        if (!muerto)
        {
            recibioDanio = true;
            VidaActual -= valorVida;

            if (VidaActual <= 0)
            {
                velocidadActal = 0;
                rbd.AddRelativeForce(new Vector3(-4, 3, 0), ForceMode.Impulse);
                lazamiento = true;
                muerto = true;
            }
        }
    }

    public void Lanzamiento(bool miLazamiento)
    {
        if (VidaActual > 0 && !lazamiento)
        {
            lazamiento = miLazamiento;
            velocidadActal = 0;
            rbd.AddRelativeForce(new Vector3(-4, 3, 0), ForceMode.Impulse);
        }
    }

    private void Recuperacion()
    {
        if (recibioDanio && !muerto && !lazamiento)
        {
            tiempoDeRecuperacion += Time.deltaTime;
            velocidadActal = 0;

            if (tiempoDeRecuperacion >= tiempoParaRecuperacion)
            {
                velocidadActal = velocidad;
                recibioDanio = false;
                tiempoDeRecuperacion = 0;
            }
        }
    }

    private void Animaciones()
    {
        anim.SetFloat("Camina", Mathf.Abs(rbd.velocity.magnitude));
        anim.SetBool("Lanzamiento", lazamiento);
        anim.SetBool("Muerto", muerto);
        anim.SetBool("Hit", recibioDanio);
    }

    private void ReiniciarVelocidad()
    {
        velocidadActal = velocidad;
    }

    private void Levantar()
    {
        lazamiento = false;
        transform.position = new Vector3(transform.position.x, 8.9f, transform.position.z);
    }

    private void DestruirObjeto()
    {
        Destroy(gameObject);
    }


}
