using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolpesEnemigo : MonoBehaviour
{
    public int danio;
    EnemigoControl enemigo;

    // Start is called before the first frame update
    void Start()
    {
        enemigo = GetComponentInParent<EnemigoControl>();
    }

    private void OnTriggerEnter(Collider other)
    {
        PersonajeControl objetivo = other.GetComponent<PersonajeControl>();

        if (objetivo != null)
        {
            objetivo.RecibirDanio(danio);

            if (enemigo.lanzarEntidad() == true)
            {
                objetivo.Lanzamiento(true);
            }
        }
    }
}
