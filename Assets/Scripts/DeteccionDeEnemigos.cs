using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeteccionDeEnemigos : MonoBehaviour
{
    private void OnTriggerStay(Collider other)
    {
        EnemigoControl enemigo = other.GetComponent<EnemigoControl>();

        if (enemigo != null)
        {
            enemigo.noMover = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        EnemigoControl enemigo = other.GetComponent<EnemigoControl>();

        if (enemigo != null)
        {
            enemigo.noMover = false;
        }
    }
}
