using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlDeCamara : MonoBehaviour
{
    public float margenX;
    public float retarzo;

    public Vector2 margenMinimo;
    public Vector2 margenMaximo;

    private Transform miPersoaje;

    private void Awake()
    {
        miPersoaje = GameObject.FindGameObjectWithTag("Player").transform;
    }

    public bool ChecarMargenEnX()
    {
        return (transform.position.x - miPersoaje.position.x) < margenX;
    }

    // Update is called once per frame
    void Update()
    {
        SeguirJugador();
    }

    private void SeguirJugador()
    {
        float marcadorX = transform.position.x;

        if (ChecarMargenEnX())
        {
            marcadorX = Mathf.Lerp(transform.position.x, miPersoaje.position.x, retarzo * Time.deltaTime);
        }

        marcadorX = Mathf.Clamp(marcadorX, margenMinimo.x, margenMaximo.x);
        transform.position = new Vector3(marcadorX, transform.position.y, transform.position.z);
    }
}
